
resource "random_id" "id" {
  byte_length = 4
  prefix      = var.project_name
}

resource "google_project" "gke-test" {
  name            = var.project_name
  project_id      = random_id.id.hex
  billing_account = var.billing_account
  org_id          = var.org_id
}

resource "google_project_service" "gcp_services" {
  for_each = toset([
    "compute.googleapis.com",
    "container.googleapis.com",
    "servicenetworking.googleapis.com",
    "containerregistry.googleapis.com",
    "sql-component.googleapis.com",
    "monitoring.googleapis.com",
    "iam.googleapis.com"
  ])

  service = each.key

  project            = google_project.gke-test.project_id
  disable_on_destroy = false
}

output "project_id" {
  value = google_project.gke-test.project_id
}
