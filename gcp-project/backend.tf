
terraform {
  backend "gcs" {
    bucket      = "rogersbernat-terraform-admin"
    prefix      = "tfstate-projects"
    credentials = "../../rogersbernat-terraform-admin.json"
  }
}
