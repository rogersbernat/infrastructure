resource "google_container_cluster" "gke-test01" {
  name                     = var.clustersname
  project                  = google_project.gke-test.project_id
  location                 = var.gcp_region
  remove_default_node_pool = true
  initial_node_count       = 1
  depends_on               = [google_project_service.gcp_services]
}

resource "google_container_node_pool" "gke-test_preemptible_nodes" {
  name       = "preemptible-node-pool"
  project    = google_project.gke-test.project_id
  location   = var.gcp_region
  cluster    = google_container_cluster.gke-test01.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-micro"

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
